#include <stdlib.h>
#include <stddef.h>

#include <cstdlib>
#include <new>
#include <iostream>

template<typename T>
class Alloc {
	public:
		typedef size_t size_type;
		typedef T* pointer;
		typedef const T* const_pointer;
		typedef T& reference;
		typedef const T& const_reference;
		typedef T value_type;
		typedef ptrdiff_t difference_type;
		
		template<typename _other>
		struct rebind {
			typedef Alloc<_other> other;
		};
		Alloc() {
		}
		~Alloc() {
		}
		Alloc(const Alloc& alloc) {}
		Alloc& operator=(const Alloc& alloc) {}
		pointer address(reference t) const {
			return &t;
		}
		const_pointer address(const_reference t) const {
			return &t;
		}
		size_type max_size() const {
			return static_cast<size_type>(-1)/sizeof(size_type);
		}
		pointer allocate(size_type n, const_pointer = 0) {
			void* p = std::malloc(n*sizeof(value_type));
			if (!p) {
				throw std::bad_alloc();
			}
			return static_cast<pointer>(p);
		}
		void deallocate(pointer t, size_type) {
			std::free(t);
		}
		void construct(pointer t, const value_type& x) {
			new(t) value_type(x);
		}
		void destroy(pointer t) {
			t->~value_type();
		}
};
