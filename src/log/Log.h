#ifndef _LOG_LOG_H_
#define _LOG_LOG_H_

#include <cstdint>
#include <memory>
#include <cstring>
#include <atomic>
#include <fstream>
#include <iostream>
#include <thread>
#include <condition_variable>
#include <mutex>
#include <list>

/**
threads -> LogInfo(xxx)
				-> new LogLine
				-> move to Logline buffer
				-> encode
				-> push to Logger buffer

deamon thread -> Logger
							-> pop from Logger buffer
							-> write buffer to file
*/

// Currently surpports threes levels
enum class LogLevel : uint8_t { INFO, WARN, CRIT };

// Log line object
class LogLine {
	public:
		LogLine(LogLevel level, char const* file, char const* function, uint32_t line);
		~LogLine() {
		}
		// Use rvalue reference to avoid temporary value capy
		LogLine(LogLine&& other_log_line) = default;
		LogLine& operator=(LogLine&&) = default;

		struct string_literal_t
		{
			explicit string_literal_t(char const* s) : m_s(s) {}
			char const* m_s;
		};

	public:
		LogLine& operator<<(char arg);
		LogLine& operator<<(int32_t arg);
		LogLine& operator<<(uint32_t arg);
		LogLine& operator<<(int64_t arg);
		LogLine& operator<<(uint64_t arg);
		LogLine& operator<<(double arg);
		LogLine& operator<<(std::string const & arg);

		template < size_t N >
		LogLine& operator<<(const char (&arg)[N]) {
				encode(string_literal_t(arg));
				return *this;
		}

		void stringify(std::ostream & os);

		template < typename Arg >
		typename std::enable_if < std::is_same < Arg, char const * >::value, LogLine& >::type
		operator<<(Arg const & arg) {
			encode(arg);
			return *this;
		}

		template < typename Arg >
		typename std::enable_if < std::is_same < Arg, char * >::value, LogLine& >::type
		operator<<(Arg const & arg) {
			encode(arg);
			return *this;
		}

	private:
		char* buffer();

		template < typename Arg >
		void encode(Arg arg);

		template < typename Arg >
		void encode(Arg arg, uint8_t type_id);

		void encode(char * arg);
		void encode(char const * arg);
		void encode(string_literal_t arg);
		void encode_c_string(char const * arg, size_t length);
		void resize_buffer_if_needed(size_t additional_bytes);
		void stringify(std::ostream & os, char * start, char const * const end);

	private:
		uint32_t m_bytes_used;
		uint32_t m_buffer_size;
		std::unique_ptr<char[]> m_heap_buffer;
		char m_stack_buffer[256 - 2 * sizeof(uint32_t) - sizeof(decltype(m_heap_buffer)) - 8 /* Reserved */];
};

template <typename Arg>
inline char* decode(std::ostream& os, char* b, Arg* dummy);

template <>
inline char * decode(std::ostream & os, char * b, LogLine::string_literal_t * dummy);

template <>
inline char * decode(std::ostream & os, char * b, char ** dummy);

// Any Buffer providing interface that BufferBase limites can be a substitution.
struct BufferBase {
	virtual ~BufferBase() = default;
	virtual bool push(LogLine&& log_line) = 0;
	virtual bool try_pop(LogLine& log_line) = 0;
};

class QueueBuffer : public BufferBase {
	public:
		struct Item {
			Item(LogLine&& logline) : log_line(std::move(logline)) {
			}
			char padding[256-sizeof(LogLine)];
			LogLine log_line;
		};
		QueueBuffer() {}
		~QueueBuffer() {}
		bool push(LogLine&& log_line) {
			std::lock_guard<std::mutex> lock(m_buffer_mutex);
			m_buffer.push_back(new Item(std::move(log_line)));
			return true;
		}
		bool try_pop(LogLine& log_line) {
			std::lock_guard<std::mutex> lock(m_buffer_mutex);
			if (m_buffer.empty()) {
				return false;	
			}
			Item *item = m_buffer.front();
			m_buffer.pop_front();
			log_line = std::move(item->log_line);
			delete item;
			return true;
		}

	private:
		std::list<Item*> m_buffer;
		std::mutex m_buffer_mutex;
};

class RingBuffer : public BufferBase {
	public:
		struct Item {
			Item(LogLine&& logline) : log_line(std::move(logline)) {}
			char padding[256-sizeof(LogLine)];
			LogLine log_line;
		};
		RingBuffer() : m_buffer(static_cast<Item*>(std::malloc(size*sizeof(Item)))), m_buffer_start(m_buffer), m_buffer_end(m_buffer) {
			for (size_t i = 0; i < size; ++i) {
				static_assert(sizeof(Item) == 256, "Unexpected size != 256");
			}	
		}
		~RingBuffer() {
			for (size_t i = 0; i < size; ++i) {
				m_buffer[i].~Item();
			}
			std::free(m_buffer);
		}
		bool push(LogLine&& log_line) {
			std::lock_guard<std::mutex> lock(m_buffer_mutex);
			if (m_buffer_end-m_buffer_start == 65536) {
				return false;
			}
			if (m_buffer_end - m_buffer == 65536) {
				m_buffer_end = m_buffer;
			}
			new(m_buffer_end) Item(std::move(log_line));
			m_buffer_end++;
			return true;
		}
		bool try_pop(LogLine& log_line) {
			std::lock_guard<std::mutex> lock(m_buffer_mutex);
			if (m_buffer_start == m_buffer_end) return false;
			if (m_buffer_start - m_buffer == 65536) {
				m_buffer_start = m_buffer;
			}
			Item& item = *m_buffer_start++;
			log_line = std::move(item.log_line);
			return true;	
		}

	private:
		size_t size = 65536;
		std::mutex m_buffer_mutex;
		Item* m_buffer;
		Item* m_buffer_start;
		Item* m_buffer_end;
};

class Buffer : public BufferBase {
	public:
		struct Item {
			Item(LogLine&& logline) : log_line(std::move(logline)) {}
			char padding[256-sizeof(LogLine)];
			LogLine log_line;
		};
		static const size_t size = 65536;
		Buffer() : m_buffer(static_cast<Item*>(std::malloc(size*sizeof(Item)))) {
			for (size_t i = 0; i < size; ++i) {
				m_write_state[i] = 0;
				static_assert(sizeof(Item) == 256, "Unexpected size != 256");
			}
		}
		~Buffer() {
			unsigned int write_count = size;
			for (size_t i = 0; i < write_count; ++i) {
				m_buffer[i].~Item();
			}
			std::free(m_buffer);
		}
		bool push(LogLine&& log_line) {
			std::lock_guard<std::mutex> lock(m_buffer_mutex);
			m_write_state[size]++;
			if (m_write_state[size]+1 == size) {
				return false;
			}
			for (size_t i = 0; i < size; ++i) {
				if (!m_write_state[i]) {
					new(&m_buffer[i]) Item(std::move(log_line));
					m_write_state[i] = 1;
					return true;
				}
			}
			return false;
		}
		bool try_pop(LogLine& log_line) {
			std::lock_guard<std::mutex> lock(m_buffer_mutex);
			for (size_t i = 0; i < size; ++i) {
				if (m_write_state[i]) {
					Item& item = m_buffer[i];
					log_line = std::move(item.log_line);
					m_write_state[i] = 0;
					return true;
				}
			}
			return false;
		}

	private:
		Item* m_buffer;
		unsigned int m_write_state[size+1];
		std::mutex m_buffer_mutex;
		std::condition_variable m_buffer_cond;
};

class Writer {
	public:
		virtual void write(LogLine& log_line) = 0;
	private:
		virtual void roll_file() = 0;	
};

class FileWriter : public Writer {
	public:
		FileWriter(std::string const &log_directory, std::string const &log_file_name, uint32_t log_file_roll_size_mb) : m_log_file_roll_size_bytes(log_file_roll_size_mb*1024*1024), m_name(log_directory+log_file_name) {
			roll_file();	
		}
		~FileWriter() {}
		virtual void write(LogLine& log_line) {}
	private:
		virtual void roll_file() {}
	private:
		uint32_t m_file_number = 0;
		uint32_t m_bytes_written = 0;
		uint32_t const m_log_file_roll_size_bytes;
		std::string const m_name;

};

class StreamWriter : public Writer {
	public:
		StreamWriter(std::string const &log_directory, std::string const &log_file_name, uint32_t log_file_roll_size_mb) : m_log_file_roll_size_bytes(log_file_roll_size_mb*1024*1024), m_name(log_directory+log_file_name) {
			roll_file();	
		}
		~StreamWriter() {
			if (m_os) {
				m_os->flush();
				m_os->close();
				delete m_os;
			}
		}
		virtual void write(LogLine& log_line) {
			auto pos = m_os->tellp();
			log_line.stringify(*m_os);
			
			m_bytes_written += m_os->tellp()-pos;
			if (m_bytes_written > m_log_file_roll_size_bytes) {
				roll_file();
			}
		}

	private:
		virtual void roll_file() {
			if (m_os) {
				m_os->flush();
				m_os->close();
			}
			m_bytes_written = 0;
			delete m_os;
			m_os = new std::ofstream();
			std::string log_file_name = m_name;
			log_file_name.append(".");
			log_file_name.append(std::to_string(++m_file_number));
			log_file_name.append(".txt");
			m_os->open(log_file_name, std::ofstream::out | std::ofstream::trunc);
		}

	private:
		uint32_t m_file_number = 0;
		uint32_t m_bytes_written = 0;
		uint32_t const m_log_file_roll_size_bytes;
		std::string const m_name;
		std::ofstream *m_os;
};

class Logger {
	public:
		Logger(std::string const &log_directory, std::string const & log_file_name, uint32_t log_file_roll_size_mb) : m_disabled(1), m_buffer(new QueueBuffer()), m_file_writer(log_directory, log_file_name, std::max(1u, log_file_roll_size_mb)), m_thread(&Logger::pop, this) {
			m_disabled.store(0, std::memory_order_release);
		}
		~Logger() {
			while (!m_destruct.load(std::memory_order_acquire));
			m_disabled.store(1);
			m_thread.join();
		}
		void add(LogLine&& log_line) {
			m_buffer->push(std::move(log_line));
		}
		void pop() {
			while(m_disabled.load(std::memory_order_acquire)) {
				std::this_thread::sleep_for(std::chrono::microseconds(50));
			}
			LogLine log_line(LogLevel::INFO, nullptr, nullptr, 0);
			while (!m_disabled.load()) {
				if (!m_destruct.load(std::memory_order_acquire)) {
					m_destruct.store(1, std::memory_order_release);
				}
				if (m_buffer->try_pop(log_line)) {
					m_file_writer.write(log_line);
				}
				else
					std::this_thread::sleep_for(std::chrono::microseconds(50));
			}
			while (m_buffer->try_pop(log_line)) {
				m_file_writer.write(log_line);
			}
		}

	private:
		std::atomic<unsigned int> m_disabled;
		std::unique_ptr<QueueBuffer> m_buffer;
		StreamWriter m_file_writer;
		std::thread m_thread;
		std::atomic<unsigned int> m_destruct{0};
};

struct Log
{
	bool operator==(LogLine&);
};
void set_log_level(LogLevel level);
bool is_logged(LogLevel level);
void initialize(std::string const &log_directory, std::string const & log_file_name, uint32_t log_file_roll_size_mb);

#define LOG(LEVEL) Log() == LogLine(LEVEL, __FILE__, __func__, __LINE__)

#define LOG_INFO is_logged(LogLevel::INFO) && LOG(LogLevel::INFO)
#define LOG_WARN is_logged(LogLevel::WARN) && LOG(LogLevel::WARN)
#define LOG_CRIT is_logged(LogLevel::CRIT) && LOG(LogLevel::CRIT)

#endif
