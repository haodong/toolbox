#include "Log.h"

typedef std::tuple < char, uint32_t, uint64_t, int32_t, int64_t, double, LogLine::string_literal_t, char * > SupportedTypes;

uint64_t timestamp_now() {
	return std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::high_resolution_clock::now().time_since_epoch()).count();
}

std::thread::id this_thread_id() {
	static thread_local const std::thread::id id = std::this_thread::get_id();
	return id;
}

char const* to_string(LogLevel log_level) {
	switch(log_level) {
		case LogLevel::INFO:
			return "INFO";
		case LogLevel::WARN:
			return "WARN";
		case LogLevel::CRIT:
			return "CRIT";
	}
	return "XXXX";
}

template < typename T, typename Tuple >
struct TupleIndex;

template < typename T,typename ... Types >
struct TupleIndex < T, std::tuple < T, Types... > > {
	static constexpr const std::size_t value = 0;
};

template < typename T, typename U, typename ... Types >
struct TupleIndex < T, std::tuple < U, Types... > > {
	static constexpr const std::size_t value = 1 + TupleIndex < T, std::tuple < Types... > >::value;
};

LogLine::LogLine(LogLevel level, char const* file, char const* function, uint32_t line) : m_bytes_used(0), m_buffer_size(sizeof(m_stack_buffer)){
	encode < uint64_t >(timestamp_now());
	encode < std::thread::id >(this_thread_id());
	encode < string_literal_t >(string_literal_t(file));
	encode < string_literal_t >(string_literal_t(function));
	encode < uint32_t >(line);
	encode < LogLevel >(level);	
}

char * LogLine::buffer() {
	return !m_heap_buffer ? &m_stack_buffer[m_bytes_used] : &(m_heap_buffer.get())[m_bytes_used];
}

template < typename Arg >
void LogLine::encode(Arg arg) {
	*reinterpret_cast<Arg*>(buffer()) = arg;
	m_bytes_used += sizeof(Arg);
}

template < typename Arg >
void LogLine::encode(Arg arg, uint8_t type_id) {
	resize_buffer_if_needed(sizeof(Arg) + sizeof(uint8_t));
	encode < uint8_t >(type_id);
	encode < Arg >(arg);
}

void LogLine::encode(char const * arg) {
	if (arg != nullptr)
		encode_c_string(arg, strlen(arg));
}

void LogLine::encode(char * arg) {
	if (arg != nullptr)
		encode_c_string(arg, strlen(arg));
}

void LogLine::encode_c_string(char const * arg, size_t length) {
	if (length == 0)
		return;

	resize_buffer_if_needed(1 + length + 1);
	char * b = buffer();
	auto type_id = TupleIndex < char *, SupportedTypes >::value;
	*reinterpret_cast<uint8_t*>(b++) = static_cast<uint8_t>(type_id);
	memcpy(b, arg, length + 1);
	m_bytes_used += 1 + length + 1;
}

void LogLine::encode(string_literal_t arg) {
	encode < string_literal_t >(arg, TupleIndex < string_literal_t, SupportedTypes >::value);
}

void LogLine::resize_buffer_if_needed(size_t additional_bytes) {
	size_t const required_size = m_bytes_used+additional_bytes;
	if (required_size <= m_buffer_size) {
		return;
	}
	if (!m_heap_buffer) {
		m_buffer_size = std::max(static_cast<size_t>(512), required_size);
		m_heap_buffer.reset(new char[m_buffer_size]);
		memcpy(m_heap_buffer.get(), m_stack_buffer, m_bytes_used);
		return;
	} else {
		m_buffer_size = std::max(static_cast<size_t>(m_buffer_size*2), required_size);
		std::unique_ptr<char[]> new_heap_buffer(new char[m_buffer_size]);
		memcpy(new_heap_buffer.get(), m_heap_buffer.get(), m_bytes_used);
		m_heap_buffer.swap(new_heap_buffer);
	}
}

void format_timestamp(std::ostream & os, uint64_t timestamp) {
	// The next 3 lines do not work on MSVC!
	// auto duration = std::chrono::microseconds(timestamp);
	// std::chrono::high_resolution_clock::time_point time_point(duration);
	// std::time_t time_t = std::chrono::high_resolution_clock::to_time_t(time_point);
	std::time_t time_t = timestamp / 1000000;
	auto gmtime = std::gmtime(&time_t);
	char buffer[32];
	strftime(buffer, 32, "%Y-%m-%d %T.", gmtime);
	char microseconds[7];
	sprintf(microseconds, "%06lu", timestamp % 1000000);
	os << '[' << buffer << microseconds << ']';
}

void LogLine::stringify(std::ostream & os) {
	char * b = !m_heap_buffer ? m_stack_buffer : m_heap_buffer.get();
	char const * const end = b + m_bytes_used;
	uint64_t timestamp = *reinterpret_cast < uint64_t * >(b); b += sizeof(uint64_t);
	std::thread::id threadid = *reinterpret_cast < std::thread::id * >(b); b += sizeof(std::thread::id);
	string_literal_t file = *reinterpret_cast < string_literal_t * >(b); b += sizeof(string_literal_t);
	string_literal_t function = *reinterpret_cast < string_literal_t * >(b); b += sizeof(string_literal_t);
	uint32_t line = *reinterpret_cast < uint32_t * >(b); b += sizeof(uint32_t);
	LogLevel loglevel = *reinterpret_cast < LogLevel * >(b); b += sizeof(LogLevel);

	format_timestamp(os, timestamp);

	os << '[' << to_string(loglevel) << ']'
		 << '[' << threadid << ']'
		 << '[' << file.m_s << ':' << function.m_s << ':' << line << "] ";

	stringify(os, b, end);

	os << std::endl;

	if (loglevel >= LogLevel::CRIT)
			os.flush();
}

template < typename Arg >
char * decode(std::ostream & os, char * b, Arg * dummy) {
	Arg arg = *reinterpret_cast < Arg * >(b);
	os << arg;
	return b + sizeof(Arg);
}

template <>
char * decode(std::ostream & os, char * b, LogLine::string_literal_t * dummy) {
	LogLine::string_literal_t s = *reinterpret_cast < LogLine::string_literal_t * >(b);
	os << s.m_s;
	return b + sizeof(LogLine::string_literal_t);
}

template <>
char * decode(std::ostream & os, char * b, char ** dummy) {
	while (*b != '\0') {
		os << *b;
		++b;
	}
	return ++b;
}

LogLine& LogLine::operator<<(std::string const & arg) {
	encode_c_string(arg.c_str(), arg.length());
	return *this;
}

LogLine& LogLine::operator<<(int32_t arg) {
	encode < int32_t >(arg, TupleIndex < int32_t, SupportedTypes >::value);
	return *this;
}

LogLine& LogLine::operator<<(uint32_t arg) {
	encode < uint32_t >(arg, TupleIndex < uint32_t, SupportedTypes >::value);
	return *this;
}

LogLine& LogLine::operator<<(int64_t arg) {
	encode < int64_t >(arg, TupleIndex < int64_t, SupportedTypes >::value);
	return *this;
}

LogLine& LogLine::operator<<(uint64_t arg) {
	encode < uint64_t >(arg, TupleIndex < uint64_t, SupportedTypes >::value);
	return *this;
}

LogLine& LogLine::operator<<(double arg) {
	encode < double >(arg, TupleIndex < double, SupportedTypes >::value);
	return *this;
}

LogLine& LogLine::operator<<(char arg) {
	encode < char >(arg, TupleIndex < char, SupportedTypes >::value);
	return *this;
}

void LogLine::stringify(std::ostream & os, char * start, char const * const end) {
	if (start == end)
	    return;

	int type_id = static_cast < int >(*start); start++;
	
	switch (type_id)
	{
	case 0:
	    stringify(os, decode(os, start, static_cast<std::tuple_element<0, SupportedTypes>::type*>(nullptr)), end);
	    return;
	case 1:
	    stringify(os, decode(os, start, static_cast<std::tuple_element<1, SupportedTypes>::type*>(nullptr)), end);
	    return;
	case 2:
	    stringify(os, decode(os, start, static_cast<std::tuple_element<2, SupportedTypes>::type*>(nullptr)), end);
	    return;
	case 3:
	    stringify(os, decode(os, start, static_cast<std::tuple_element<3, SupportedTypes>::type*>(nullptr)), end);
	    return;
	case 4:
	    stringify(os, decode(os, start, static_cast<std::tuple_element<4, SupportedTypes>::type*>(nullptr)), end);
	    return;
	case 5:
	    stringify(os, decode(os, start, static_cast<std::tuple_element<5, SupportedTypes>::type*>(nullptr)), end);
	    return;
	case 6:
	    stringify(os, decode(os, start, static_cast<std::tuple_element<6, SupportedTypes>::type*>(nullptr)), end);
	    return;
	case 7:
	    stringify(os, decode(os, start, static_cast<std::tuple_element<7, SupportedTypes>::type*>(nullptr)), end);
	    return;
	}
}

std::unique_ptr<Logger> logger;
std::atomic<Logger*> atomic_logger;
std::atomic < unsigned int > loglevel = {0};

bool Log::operator==(LogLine& log_line) {
	atomic_logger.load(std::memory_order_acquire)->add(std::move(log_line));
	return true;
}

bool is_logged(LogLevel level) {
	return static_cast<unsigned int>(level) >= loglevel.load(std::memory_order_relaxed);
}

void set_log_level(LogLevel level) {
	loglevel.store(static_cast<unsigned int>(level), std::memory_order_release);
}

void initialize(std::string const &log_directory, std::string const & log_file_name, uint32_t log_file_roll_size_mb) {
	logger.reset(new Logger(log_directory, log_file_name, log_file_roll_size_mb));
	atomic_logger.store(logger.get(), std::memory_order_seq_cst);
}

