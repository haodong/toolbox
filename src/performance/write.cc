#include "unistd.h"
#include "fcntl.h"
#include "string.h"
#include "stdio.h"

#include <stdlib.h>

#include <chrono>
#include <unordered_map>

#define DATA_LEN (1024*1024*20)

uint64_t timestamp_now() {
  return std::chrono::high_resolution_clock::now().time_since_epoch() / std::chrono::microseconds(1);
}

void test_direct_write() {
  ssize_t fd = ::open("tmp_write_direct", O_RDWR|O_CREAT|O_DIRECT, 0644);
  char *chr;
  ssize_t res;
  res = ::posix_memalign((void**)&chr, getpagesize(), DATA_LEN);
  if (res != 0) {
    fprintf(stderr, "error on possix_memalign...\n");
  }
  ::memset(chr, 'a', DATA_LEN);
  res = ::write(fd, chr, strlen(chr));
  if (res < 0) {
    fprintf(stderr, "error on write...\n");
  }
  ::close(fd);
  ::free(chr);
  chr = NULL;
}

void test_write() {
  ssize_t fd = ::open("tmp_write", O_RDWR|O_CREAT, 0644);
  char *chr;
  chr = (char*)::malloc(DATA_LEN*sizeof(char));
  memset(chr, 'a', DATA_LEN);
  ssize_t res;
  res = write(fd, chr, DATA_LEN);
  if (res < 0) {
    fprintf(stderr, "error on write...\n");
  }
  ::close(fd);
}

void test_map() {
   std::unordered_map<int,int> m;
   m.reserve(DATA_LEN);
   for (int i = 0; i < DATA_LEN; ++i)
      m[i] = i;
}

int main() {
  uint64_t begin = timestamp_now();
  test_direct_write();
  uint64_t end = timestamp_now();
  long int latency = (end-begin) * 1000;
  printf("\tDirect write spend time: %ld nanoseconds\n", latency);

  begin = timestamp_now();
  test_write();
  end = timestamp_now();
  latency = (end-begin) * 1000;
  printf("\tNornal write spend time: %ld nanoseconds\n", latency);

  begin = timestamp_now();
  test_map();
  end = timestamp_now();
  latency = (end-begin) * 1000;
  printf("\tMap write spend time: %ld nanoseconds\n", latency);

  return 0;
}
